import React from 'react';

function Tag({children, ...rest}) {
  return <div {...rest}>{children}</div>
}

export default Tag;
